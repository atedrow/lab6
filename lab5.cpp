/******************
 * Alexander Tedrow
 * atedrow
 * lab5
 * CpSc 1021 002 Spring 2019
 * Hanjie Liu
******************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  int i, j, k;
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
  Card deck[52];
  for(i = 0, k=0; i < 4; i++) {
    for(j = 0; j < 13; j++) {
      deck[k].suit = static_cast<Suit>(i);
      deck[k].value = j+2;
      k++;
    }
  }

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
  random_shuffle(&deck[0], &deck[52], myrandom);

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
  Card hand[5] = {deck[0], deck[1], deck[2], deck[3], deck[4]};

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
  sort(&hand[0], &hand[5], suit_order);


    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
  for(i = 0; i < 5; i++) {
    cout<< setw(10);
    if(hand[i].value > 10) {
      cout << get_card_name(hand[i]);
    }else{
      cout << hand[i].value;
    }
    cout << " of " << get_suit_code(hand[i]) << endl;
  }


  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  if(lhs.suit < rhs.suit) {
    return true;
  }else if(lhs.suit == rhs.suit) {
    if(lhs.value < rhs.value) {
      return true;
    }else{
      return false;
    }
  }else{
    return false;
  }
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  switch(c.value) {
    case 11: return "Jack"; break;
    case 12: return "Queen"; break;
    case 13: return "King"; break;
    case 14: return "Ace"; break;
    default: return ""; break;
  }
}
